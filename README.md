# Vimeo Downloader
Simple web extension that just adds a download link to the Vimeo videos.

## Installation
TODO

## Contributing
If you want to contribute, start with the [issue tracker](https://gitlab.com/uxmaster/vimeo-downloader/-/issues), please.
Ether pick up an issue to resolve or report a new one to discuss the way to go.

## Acknowledgments
The extension is based on the step-by-step guide at [How To Download Private Vimeo Videos in 10 seconds](https://viddownmadness.com/download-private-vimeo-videos/#steps).
The download icon by [Vaneet Thakur](https://www.svgrepo.com/author/Vaneet%20Thakur/) is from [SVG repo](https://www.svgrepo.com/svg/513826/download).

## License
Do whatever you want to (WTFPL analog without the fuck).
But I'm not liable for anything.


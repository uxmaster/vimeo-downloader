const scripts = document.getElementsByTagName('script');
let script;
for (script of scripts) {
    if (script.textContent.indexOf('window.playerConfig') > -1) break;
}
try {
    const playerConfig = script.textContent.match(/window.playerConfig = ({.*}})/)[1];

    const config = JSON.parse(playerConfig);
    const files = config.request.files.progressive;
    const file = files.reduce((bestRes, currentRes) => bestRes.width > currentRes.width ? bestRes : currentRes);

    const style = document.createElement('style');
    style.appendChild(document.createTextNode(`
    a.vimeo-downloader {
        position: absolute;
        top: .8em;
        right: .8em;
        background-color: rgba(0,0,0,0.9);
        border-radius: 4px;
        padding: .2em;
        height: 24px;
        opacity: 0;
        transition: opacity 0.25s linear;
        z-index: 99999;
    }
    body:hover > a.vimeo-downloader {
        opacity: 1;
    }
    `));
    document.head.appendChild(style);
    
    const a = document.createElement('a');
    a.className = 'vimeo-downloader';
    a.title = 'Download';
    a.innerHTML = `
<svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" style="">
    <g id="Complete">
        <g id="download">
            <g>
                <path d="M3,12.3v7a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2v-7" fill="none" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                <g>
                    <polyline data-name="Right" fill="none" id="Right-2" points="7.9 12.3 12 16.3 16.1 12.3" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                    <line fill="none" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" x1="12" x2="12" y1="2.7" y2="14.2"/>
                </g>
            </g>
        </g>
    </g>
</svg>`;
    a.href = file.url;
    a.onclick = () => {
        browser.runtime.sendMessage({
            url: file.url,
            filename: `${config.video.title.replace(/[:\\<>/!@?"*.|\n\r\t]/g, ' ')}.mp4`
        });
        return false;
    };

    document.body.appendChild(a);
}
catch {}
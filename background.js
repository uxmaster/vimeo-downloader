console.log('background.js');

function handleMessage(request) {
	const downloading = browser.downloads.download({
		url : request.url,
		filename : request.filename,
		conflictAction : 'uniquify'
	});
}
  
browser.runtime.onMessage.addListener(handleMessage);